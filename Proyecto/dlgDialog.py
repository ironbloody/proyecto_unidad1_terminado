#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from dlg import dlgAxiliar


class dlgVisualize():
    def __init__(self, directory, name, titulo=""):
        self.directory = directory
        self.name = name
        cont = 0
        cont2 = 0
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")
        # Se agrega titulo
        self.visualize = self.builder.get_object("visualizeProt")
        self.visualize.set_title("Protein Information")
        self.visualize.resize(450, 300)
        self.visualize.show_all()

        # label
        self.label_visualize_prot = self.builder.get_object("labelvisualizeProt")

        # botones
        boton_quit = self.builder.get_object("btnQuit")
        boton_quit.connect("clicked", self.boton_quit_clicked)

        boton_save = self.builder.get_object("btnSave")
        boton_save.connect("clicked", self.boton_save_clicked)

        # Comboboxtext
        lista = ["ATOM", "HETATM", "ANISOU", "OTHERS"]
        self.comboboxtext = self.builder.get_object("comboboxtext")
        self.comboboxtext.connect("changed", self.comboboxtext_changed)
        self.comboboxtext.set_entry_text_column(0)
        # Se agregan las opciones al comboboxtex
        for index in lista:
            self.comboboxtext.append_text(index)
        # a partir de la ruta + el nombre del archivo se obtiene solo la ruta
        for index in self.directory:
            if index == "/":
                cont += 1
            self.text = ""
        for index in self.directory:
            if cont2 == cont-1:
                break
            if index == "/":
                cont2 += 1
            self.text += index
        self.complete_directory = self.text + self.name

    def boton_quit_clicked(self, btn=None):
        self.visualize.destroy()

    def boton_save_clicked(self, btn=None):
        ppdb = PandasPdb()
        ppdb.read_pdb(self.directory)
        if self.pdb_column == "ATOM":
            # si existe ATOM se guarda la informacion sino llama a dlgAuxiliar
            if ppdb.df["ATOM"].empty is True:
                dlgAxiliar(object_name="noGuardar")
            else:
                ppdb.to_pdb(path=self.complete_directory + "_ATOM.pdb", records=['ATOM'], gz=False, append_newline=True)
                dlg = dlgAxiliar(object_name="archivoGuardar")
        if self.pdb_column == "HETATM":
            # si existe HETATM se guarda la informacion sino llama a dlgAuxiliar
            if ppdb.df["HETATM"].empty is True:
                dlg = dlgAxiliar(object_name="noGuardar")
            else:
                ppdb.to_pdb(path=self.complete_directory + "_HETATM.pdb", records=['HETATM'], gz=False, append_newline=True)
                dlg = dlgAxiliar(object_name="archivoGuardar")
        if self.pdb_column == "ANISOU":
            # si existe ANISOU se guarda la informacion sino llama a dlgAuxiliar
            if ppdb.df["ANISOU"].empty is True:
                self.label_visualize_prot.set_text("NO VAlUE")
                dlg = dlgAxiliar(object_name="noGuardar")
            else:
                ppdb.to_pdb(path=self.complete_directory + "_ANISOU.pdb", records=["ANISOU"], gz=False, append_newline=True)
                dlg = dlgAxiliar(object_name="archivoGuardar")
        if self.pdb_column == "OTHERS":
            # si existe OTHERS se guarda la informacion sino llama a dlgAuxiliar
            if ppdb.df["OTHERS"].empty is True:
                self.label_visualize_prot.set_text("NO VAlUE")
                dlg = dlgAxiliar(object_name="noGuardar")
            else:
                ppdb.to_pdb(path=self.complete_directory + "_OTHERS.pdb", records=['OTHERS'], gz=False, append_newline=True)
                dlg = dlgAxiliar(object_name="archivoGuardar")

    def comboboxtext_changed(self, cmb=None):
        ppdb = PandasPdb()
        ppdb.read_pdb(self.directory)
        # Se obtiene la imformacion pdb
        self.valor_comboboxtext = self.comboboxtext.get_active_text()
        self.pdb_column = "".join([self.valor_comboboxtext])
        text = ppdb.pdb_text[:1000]
        # Se filtra segun las columnas
        # Si existe ATOM se muestra toda la imforacion sino un NO VALUE
        if self.pdb_column == "ATOM":
            if ppdb.df["ATOM"].empty is True:
                self.label_visualize_prot.set_text("NO VAlUE")
            else:
                self.label_visualize_prot.set_text(str(ppdb.df["ATOM"].to_string()))
        # Si existe HETATM se muestra toda la imforacion sino un NO VALUE
        if self.pdb_column == "HETATM":
            if ppdb.df["HETATM"].empty is True:
                self.label_visualize_prot.set_text("NO VAlUE")
            else:
                self.label_visualize_prot.set_text(str(ppdb.df["HETATM"].to_string()))
        # Si existe ANISOU se muestra toda la imforacion sino un NO VALUE
        if self.pdb_column == "ANISOU":
            if ppdb.df["ANISOU"].empty is True:
                self.label_visualize_prot.set_text("NO VAlUE")
            else:
                self.label_visualize_prot.set_text(str(ppdb.df["ANISOU"].to_string()))
        # Si existe OTHERS se muestra toda la imforacion sino un NO VALUE
        if self.pdb_column == "OTHERS":
            if ppdb.df["OTHERS"].empty is True:
                self.label_visualize_prot.set_text("NO VAlUE")
            else:
                self.label_visualize_prot.set_text(str(ppdb.df["OTHERS"].to_string()))


