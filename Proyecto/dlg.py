#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgAxiliar():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")
        self.dialogo = self.builder.get_object(object_name)
        # Solo si es la ventada de file chooser se crea el boton ok

        if object_name == "dlgFileChooser":
            boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                    Gtk.ResponseType.OK)
            boton_aceptar.set_always_show_image(True)
            boton_aceptar.connect("clicked", self.boton_open_clicked)

        if object_name == "noGuardar" or object_name == "archivoGuardar":
            boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                    Gtk.ResponseType.OK)
            boton_aceptar.set_always_show_image(True)
            boton_aceptar.connect("clicked", self.boton_ok)

        self.dialogo.show_all()

    # Retorna la ruta del folder
    def boton_open_clicked(self, btn=None):
        self.ruta = self.dialogo.get_current_folder()
        return self.ruta

    def boton_ok(self, btn=None):
        self.dialogo.destroy()

