#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import gi
import  pymol
import  __main__
# Pymol: quiet  and no GUI
__main__.pymol_argv = ['pymol', '-qc']
pymol.finish_launching ()
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlg import dlgAxiliar
from dlgDialog import dlgVisualize
from biopandas.pdb import PandasPdb


class Principal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.window = self.builder.get_object("wnMain")
        self.window.set_title("Protein Visualizer")
        self.window.resize(600, 400)
        self.window.connect("destroy", Gtk.main_quit)

        # Gtk image
        self.img = self.builder.get_object("openimagen")

        # Boton acerca de
        boton_about = self.builder.get_object("btnAbout")
        boton_about.connect("clicked", self.boton_about_clicked)

        # Boto abrir
        boton_open = self.builder.get_object("btnOpen")
        boton_open.connect("clicked", self.boton_open_clicked)

        # Boton visualizar
        boton_visualize = self.builder.get_object("btnVisualize")
        boton_visualize.connect("clicked", self.boton_visualize_clicked)

        # Label
        self.label_ejemplo = self.builder.get_object("labelEjemplo")
        self.label_carac = self.builder.get_object("labelCarac")

        # Tree
        self.tree = self.builder.get_object("treeEjemplo")
        self.tree_model = Gtk.ListStore(*([str]))
        self.tree.set_model(model=self.tree_model)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Proteinas",
                                    cell_renderer=cell,
                                    text=0)

        self.tree.append_column(column)

        self.tree.connect("cursor-changed", self.liststore_changed)

        # Label
        self.label = self.builder.get_object("label")

        self.direcciones = []
        self.archivos_pdb = []
        self.nombres_proteinas = []
        self.impresion = []

        self.window.show_all()

    def liststore_changed(self, tree=None):
        # El usuario tiene que tener seleccionada una proteina
        tree_model, it = self.tree.get_selection().get_selected()
        if tree_model is None or it is None:
            return
        # Se guarda en impresion la proteina que selecciono el usuario
        self.impresion.append(tree_model.get_value(it, 0))
        self.label.set_text(self.ruta+"/"+tree_model.get_value(it, 0)+".pdb")
        self.crear_imagen()

    def limpiar_listore(self):
        # Se la listore para que pueda abrir otra vez la capeta
        self.tree_model.clear()
        self.nombres_proteinas.clear()
        self.archivos_pdb.clear()

    def boton_open_clicked(self, btn=None):
        # Se llama al file chooser
        self.limpiar_listore()
        dlg = dlgAxiliar(object_name="dlgFileChooser")
        response = dlg.dialogo.run()
        # Se obtien la ruta de la carpeta
        self.ruta = dlg.boton_open_clicked(self)
        dlg.dialogo.destroy()
        # Se crea una lista con todos los archivos de la ruta obtenida
        self.direcciones = os.listdir(self.ruta)
        # se filtran solo los archivos ".pdb"
        for i in self.direcciones:
            if os.path.isfile(os.path.join(self.ruta, i)) and i.endswith(".pdb"):
                self.archivos_pdb.append(i)
        # Se obtiene una lista solo con los nombres de las proteinas
        for i in self.archivos_pdb:
            texto = ""
            for j in i:
                if j == ".":
                    break
                texto += j
            self.nombres_proteinas.append(texto)
        # Se llena el listore con los nombres de las proteinas
        self.llenar_liststore()
        dlg.dialogo.destroy()

    def llenar_liststore(self):
        # Se llena el ListStore con los nombres de las proteinas
        for i in self.nombres_proteinas:
            self.tree_model.append([i])

    def boton_about_clicked(self, btn=None):
        # abrir el acerca de
        dlg = dlgAxiliar(object_name="dlgAbout")
        dlg.dialogo.run()
        dlg.dialogo.destroy()

    def boton_visualize_clicked(self, btn=None):
        # el usuario tiene que tener seleccionada una opcion del ListStore
        tree_model, it = self.tree.get_selection().get_selected()
        if tree_model is None or it is None:
            return
        # Se llama al dialogo y se le envia la ruta y el nombre de la proteina
        directory = self.pdb_file
        name = self.pdb_name
        dlg = dlgVisualize(directory, name)

    def imprimir_imagen(self):
        # se crea la ruta y se le envia al gtk image
        archivo_png = self.ruta + "/" + self.impresion[0] + ".png"
        self.img.set_from_file(archivo_png)
        # Se limpia
        self.impresion.clear()

    def crear_imagen(self):
        # Se crea la ruta y el nombre de la proteina .png
        self.pdb_file = self.ruta + "/" + self.impresion[0] + ".pdb"
        self.pdb_name = self.impresion[0]

        pymol.cmd.load(self.pdb_file, self.pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(self.pdb_name)
        pymol.cmd.get_names()
        pymol.cmd.hide('all')
        pymol.cmd.show('cartoon')
        pymol.cmd.set('ray_opaque_background', 0)
        pymol.cmd.pretty(self.pdb_name)
        pymol.cmd.png(self.ruta + "/" + self.pdb_name + ".png")
        pymol.cmd.ray()
        # Una vez creada la imagen se llama a la funcion que la imprime
        self.imprimir_imagen()
        # Se llama a pandas y se imprimen los primeros 1000 caracteres
        ppdb = PandasPdb()
        ppdb.read_pdb(self.pdb_file)
        text = ppdb.pdb_text[:1000]
        self.label_carac.set_text(text)


if __name__ == "__main__":
    PRINCIPAL = Principal()
    Gtk.main()

