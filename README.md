**Visualizador de proteínas Pdb Usando Biopandas y Pymol**                                                                                             
(V.P.P.U.B.P)

Programa basado en programación orientada a objetos e interfases graficas                                                                              

**Pre-requisitos**                                                                                                                                    
Python3                                                                                                                                                
Python3-gi                                                                                                                                             
Glade                                                                                                                                                  
Pymol                                                                                                                                                  
Biopandas                                                                                                                                              

**Instalación**                                                                                                                                       
Se recomienda que cada vez que se prenda su equipo, abrir la terminal y usar sudo apt-get update para evitar problemas.                                

Para instalar Python3 inicie la terminal y escriba lo siguiente.                                                                                       
sudo apt-get install python3.6                                                                                                                         

Para instalar Python3-gi inicie la terminal y escriba lo siguiente.                                                                                    
su –                                                                                                                                                   
apt-cache search python3-gi                                                                                                                            
apt install python3-gi                                                                                                                                             

Para instalar Glade inicie la terminal y escriba lo siguiente.                                                                                         
apt install glade                                                                                                                                      

Para instalar Pymol inicie la terminal y escriba lo siguiente.                                                                                         
apt-cache search pymol                                                                                                                                 
sudo apt-get install pymol                                                                                                                             

Para instalar Biopandas inicie la terminal y escriba lo siguiente.                                                                                     
pip3 install biopandas                                                                                                                             

**Ejecutando**                                                                                                                                         
Al abrir el programa se encontrará con una ventana que tendrá 3 botones, Acerca de, Abrir y Visualizar, Para comenzar pulse el botón de Abrir, este le abrirá una nueva ventana en la cual debe elegir el directorio donde tiene guardado los archivos pdb, debe entrar a esta carpeta y pulsar aceptar. Luego de esto vera en la ventana principal todos los archivos pdb que contenía la carpeta, una vez aquí si selecciona cualquiera de estos, le mostrara la imagen de esa proteína junto con los primeros 1000 caracteres. Ahora si lo desea puede pulsar el botón de visualizar el cual le abrirá otra ventana donde puede seleccionar 4 distintas opciones, ATOM, HETATM, ANISOU y OTHERS, cada una le mostrará su respectiva información y si lo desea podrá guardarla en un archivo de texto por separado con el botón guardar.

**Nota**                                                                                                                                               
Si desea ver la versión, nombre y los desarrolladores del programa pulse el botón “Acerca de” el cual le abrirá una ventana con toda esta información.

**Construido con**                                                                                                                                    
Glade – Interfas Grafica                                                                                                                               
Biopanda – Usado para crear la información de ATOM, HETATM, ANISOU y OTHERS                                                                            
Pymol – Usado para generar la imagen                                                                                                                   

**Versionado**                                                                                                                                        
Version 1.77013                                                                                                                                        

**Autores**                                                                                                                                           
Luis Rebolledo – Construcción Pymol                                                                                                                    
Rodrigo Valenzuela – Construcción Biopanda                                                                                                             

**Licencia **                                                                                                                                          
Este proyecto está bajo la Licencia Publica General de GNU, versión 3 o posterior.                                                                     
